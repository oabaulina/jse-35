package ru.baulina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.listener.AbstractListener;

import java.util.List;

public interface ICommandRepository {

    void add(@NotNull AbstractListener command);

    void remove(@NotNull AbstractListener command);

    @NotNull
    List<AbstractListener> findAll();

    @NotNull
    List<String> getCommandsNames();

    @NotNull
    List<String> getArgs();

    void clear();

    @Nullable
    AbstractListener getByArg(@NotNull String arg);

    @Nullable
    AbstractListener getByName(@NotNull String name);

    @Nullable
    AbstractListener removeByName(@NotNull String name);

    @Nullable
    AbstractListener removeByArg(@NotNull String name);

}
