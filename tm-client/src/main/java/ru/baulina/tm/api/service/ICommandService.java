package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.listener.AbstractListener;

import java.util.List;

public interface ICommandService {

    void add(@Nullable final AbstractListener command);

    void remove(@Nullable final AbstractListener command);

    @NotNull
    List<AbstractListener> findAll();

    @NotNull
    List<String> getCommandsNames();

    @NotNull
    List<String> getArgs();

    void clear();

    @Nullable
    AbstractListener getByArg(@Nullable final String arg);

    @Nullable
    AbstractListener getByName(@Nullable final String name);

    @Nullable
    AbstractListener removeByName(@Nullable final String name);

    @Nullable
    AbstractListener removeByArg(@Nullable final String name);

}
