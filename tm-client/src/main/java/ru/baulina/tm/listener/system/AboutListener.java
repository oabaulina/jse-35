package ru.baulina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.listener.AbstractListener;

@Component
public class AboutListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@aboutListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        about();
    }

    @Async("asyncExecutor")
    @EventListener(condition = "@aboutListener.arg() == #event.name")
    public void handlerArg(@NotNull final ConsoleEvent event) {
        about();
    }

    private void about() {
        System.out.println("[ABOUT]");
        System.out.println("Olga Baulina");
        System.out.println("golovolomkacom@gmail.com");
        System.out.println("OK");
        System.out.println();
    }

}
