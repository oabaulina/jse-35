package ru.baulina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.listener.AbstractListener;

@Component
public class ExitListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@exitListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.exit(0);
    }

    @Async("asyncExecutor")
    @EventListener(condition = "@exitListener.arg() == #event.name")
    public void handlerArg(@NotNull final ConsoleEvent event) {
        System.exit(0);
    }

}
