package ru.baulina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.listener.AbstractListener;
import ru.baulina.tm.util.NumberUtil;
import ru.baulina.tm.util.SystemInformation;

@Component
public class InfoListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @NotNull
    @Override
    public String description() {
        return "Display information about system.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@infoListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        info();
    }

    @Async("asyncExecutor")
    @EventListener(condition = "@infoListener.arg() == #event.name")
    public void handlerArg(@NotNull final ConsoleEvent event) {
        info();
    }

    private void info() {
        System.out.println("[INFO]");

        System.out.println("Available processors (cores): " + SystemInformation.getAvailableProcessors());
        System.out.println("Free memory: " + NumberUtil.formatBytes(SystemInformation.getFreeMemory()));
        System.out.println("Maximum memory: " + NumberUtil.formatBytes(SystemInformation.getMaxMemory()));
        System.out.println(
                "Total memory available to JVM: " + NumberUtil.formatBytes(SystemInformation.getTotalMemory())
        );
        System.out.println(
                "Used memory by JVM: " + NumberUtil.formatBytes(SystemInformation.getUsedMemoryFormat())
        );
        System.out.println("OK");
        System.out.println();
    }

}
