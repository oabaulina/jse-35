package ru.baulina.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.TaskDTO;
import ru.baulina.tm.endpoint.TaskEndpoint;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class TaskUpdateByIdListener extends AbstractTaskListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@taskUpdateByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE TASK]");
        @Nullable final Long id;
        @Nullable final Long projectId;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER ID:");
            id = TerminalUtil.nexLong();
            System.out.println("ENTER PROJECT ID:");
            projectId = TerminalUtil.nexLong();
        }
        @Nullable final SessionDTO session = getSession();
        @Nullable final TaskDTO task = taskEndpoint.findOneTaskById(
                session, projectId, session.getUserId()
        );
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        @Nullable final String name;
        @Nullable final String description;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER NAME:");
            name = TerminalUtil.nextLine();
            System.out.println("ENTER DESCRIPTION:");
            description = TerminalUtil.nextLine();
        }
        taskEndpoint.updateTaskById(
               session, projectId, id, name, description
        );
        System.out.println("[OK]");
        System.out.println();
    }

}
