package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.listener.AbstractListener;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractListener> commands = new LinkedHashMap<>();

    @Override
    public void add(@NotNull final AbstractListener command) {
        @NotNull final String name = command.name();
        commands.put(name, command);
    }

    @Override
    public void remove(@NotNull final AbstractListener command) {
        @NotNull final String name = command.name();
        commands.remove(name);
    }

    @NotNull
    @Override
    public List<AbstractListener> findAll() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public List<String> getCommandsNames() {
        return new ArrayList<>(commands.keySet());
    }

    @NotNull
    @Override
    public List<String> getArgs() {
        @NotNull final List<String> Arg = new ArrayList<>();
        for (@NotNull final AbstractListener command : commands.values()) {
            if (command.arg() == null) continue;
            Arg.add(command.arg());
        }
        return Arg;
    }

    @Override
    public void clear() {
        commands.clear();
    }

    @Nullable
    @Override
    public AbstractListener getByArg(@NotNull final String arg) {
        for (@NotNull final AbstractListener command : commands.values()) {
            if (arg.equals(command.arg())) {
                return command;
            }
        }
        return null;
    }

    @Nullable
    @Override
    public AbstractListener getByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Nullable
    @Override
    public AbstractListener removeByName(@NotNull final String name) {
        return commands.remove(name);
    }

    @Nullable
    @Override
    public AbstractListener removeByArg(@NotNull final String arg) {
        return null;
    }

}
