package ru.baulina.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.exception.empty.EmptyNameException;
import ru.baulina.tm.exception.system.EmptyArgumentException;
import ru.baulina.tm.listener.AbstractListener;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CommandService implements ICommandService {

    @NotNull
    @Autowired
    private final ICommandRepository commandRepository;

    @Override
    public void add(@Nullable final AbstractListener command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public void remove(@Nullable final AbstractListener command) {
        if (command == null) return;
        commandRepository.remove(command);
    }

    @NotNull
    @Override
    public List<AbstractListener> findAll() {
        return commandRepository.findAll();
    }

    @NotNull
    @Override
    public List<String> getCommandsNames() {
        return commandRepository.getCommandsNames();
    }

    @NotNull
    @Override
    public List<String> getArgs() {
        return commandRepository.getArgs();
    }

    @Override
    public void clear() {
        commandRepository.clear();
    }

    @Nullable
    @Override
    public AbstractListener getByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        return commandRepository.getByArg(arg);
    }

    @Nullable
    @Override
    public AbstractListener getByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return commandRepository.getByName(name);
    }

    @Nullable
    @Override
    public AbstractListener removeByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return commandRepository.removeByName(name);
    }

    @Nullable
    @Override
    public AbstractListener removeByArg(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) throw new EmptyArgumentException();
        return commandRepository.removeByArg(arg);
    }

}
