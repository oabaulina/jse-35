package ru.baulina.tm.api.endpoint;

import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.UserDTO;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ISessionEndpoint {

    @WebMethod
    SessionDTO openSession(
            @WebParam(name = "login", partName = "login") String login,
            @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    void closeSession(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    void closeAllSession(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    UserDTO getUser(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    List<Session> getListSession(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

}
