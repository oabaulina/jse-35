package ru.baulina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.enumerated.Role;

import java.util.List;

public interface ISessionService {

    @Nullable Session open(@Nullable final String login, @Nullable final String password);

    void close(@Nullable final SessionDTO session);

    void closeAll(@Nullable final SessionDTO session);

    boolean isValid(@Nullable final SessionDTO session);

    void validate(@Nullable final SessionDTO session);

    void validate(@Nullable final SessionDTO session, @Nullable final Role role);

    boolean isTimeOut(@NotNull Long timestamp);

    @Nullable
    List<Session> getListSession(@Nullable final SessionDTO session);

    @Nullable
    String sign(@Nullable final SessionDTO session);

    boolean checkDataAccess(@Nullable final String login, @Nullable final String password);

    void signOutByLogin(@Nullable final String login);

    void signOutByUserId(@Nullable final Long userId);

}
