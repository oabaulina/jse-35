package ru.baulina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    Task create(@Nullable User user,
                @Nullable Project project,
                @Nullable String name
    );

    @Nullable
    Task create(
            @Nullable User user, @Nullable Project project,
            @Nullable String name, @Nullable String description
    );

    @Nullable
    List<Task> findAll(@Nullable final Long userId);

    void clear(@Nullable final Long userId);

    @Nullable List<Task> findListTasks();

    @Nullable
    Task findOneById(
            @Nullable Long userId,
            @Nullable Long projectId,
            @Nullable Long id
    );

    @Nullable
    Task findOneByName(
            @Nullable Long userId,
            @Nullable Long projectId,
            @Nullable String name
    );

    void remove(@Nullable Task task);

    void removeOneById(
            @Nullable final Long userId,
                       @Nullable final Long projectId,
                       @Nullable Long id);

    void removeOneByName(
            @Nullable Long userId,
            @Nullable Long projectId,
            @Nullable String name
    );

    void updateTaskById(
            @Nullable Long userId, @Nullable Long projectId,
            @Nullable Long id,
            @Nullable String name, @Nullable String description
    );

}
