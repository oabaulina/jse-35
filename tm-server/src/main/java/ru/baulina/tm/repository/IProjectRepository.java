package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void deleteByUserId(@NotNull final Long userId);

    default
    void clear(@NotNull final Long userId) {
        deleteByUserId(userId);
    };

    default
    @NotNull
    List<Project> findListProjects() {
        return findAll();
    };

    @NotNull
    List<Project> findByUserId(@Nullable final Long userId);

    default
    @NotNull
    List<Project> findAllByUserId(@Nullable final Long userId) {
        return findByUserId(userId);
    };

    @Nullable
    Project findByUserIdAndId(
            @NotNull final Long userId,
            @NotNull final Long id
    );

    default
    @Nullable
    Project findOneById(
            @NotNull final Long userId,
            @NotNull final Long id
    ) {
        return findByUserIdAndId(userId, id);
    };

    @Nullable
    Project findByUserIdAndName(
            @NotNull final Long userId,
            @NotNull final String name
    );

    default
    @Nullable
    Project findOneByName(
            @NotNull final Long userId,
            @NotNull final String name
    ) {
        return findByUserIdAndName(userId, name);
    };

    @Nullable
    Project deleteByUserIdAndId(
            @NotNull final Long userId,
            @NotNull final Long id
    );

    default
    @Nullable
    Project removeOneById(
            @NotNull final Long userId,
            @NotNull final Long id
    ) {
        return deleteByUserIdAndId(userId, id);
    };

    @Nullable
    Project deleteByUserIdAndName(
            @NotNull final Long userId,
            @NotNull final String name
    );

    default
    @Nullable
    Project removeOneByName(
            @NotNull final Long userId,
            @NotNull final String name
    ) {
        return deleteByUserIdAndName(userId, name);
    }

}
