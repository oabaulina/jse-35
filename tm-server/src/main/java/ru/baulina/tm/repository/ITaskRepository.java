package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void deleteByUserId(@NotNull final Long userId);

    default
    void clear(@NotNull final Long userId) {
        deleteByUserId(userId);
    };

    @NotNull
    List<Task> findByUserId(@NotNull final Long userId);

    default
    @NotNull
    List<Task> findAllByUserId(@NotNull final Long userId) {
        return findByUserId(userId);
    };

    default
    @NotNull
    List<Task> findListTasks() {
        return findAll();
    };

    @Nullable
    Task findByUserIdAndProjectIdAndId(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Long id
    );

    default
    @Nullable
    Task findOneById(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Long id
    ) {
        return findByUserIdAndProjectIdAndId(userId, projectId, id);
    };

    @Nullable
    Task findByUserIdAndProjectIdAndName(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final String name
    );

    default
    @Nullable
    Task findOneByName(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final String name
    ) {
        return findByUserIdAndProjectIdAndName(userId, projectId, name);
    };

    @Nullable
    Task deleteByUserIdAndProjectIdAndId(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Long id
    );

    default
    @Nullable
    Task removeOneById(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final Long id
    ) {
        return deleteByUserIdAndProjectIdAndId(userId, projectId, id);
    };

    @Nullable
    Task deleteByUserIdAndProjectIdAndName(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final String name
    );

    default
    @Nullable
    Task removeOneByName(
            @NotNull final Long userId,
            @NotNull final Long projectId,
            @NotNull final String name
    ) {
        return deleteByUserIdAndProjectIdAndName(userId, projectId, name);
    }

}
