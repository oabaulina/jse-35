package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.exception.entity.ProjectNotFoundException;
import ru.baulina.tm.repository.IProjectRepository;

import java.util.List;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    protected IProjectRepository projectRepository;

    @NotNull
    @Autowired
    protected ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(
            @Nullable final User user,
            @Nullable final String name
    ) {
        if (user == null) throw new EmptyUserException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setUser(user);
        project.setName(name);
        projectRepository.save(project); //merge
        return project;
    }

    @Nullable
    @Override
    @Transactional
    public Project create(
            @Nullable final User user,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (user == null) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setUser(user);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project); //merge
        return project;
    }

    @Override
    @Transactional
    public void clear(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public List<Project> findAll(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        return projectRepository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public List<Project> findListProjects() {
        return projectRepository.findListProjects();
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public Project findOneById(
            @Nullable final Long userId,
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public Project findOneByName(
            @Nullable final Long userId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    @Transactional
    public void remove(@Nullable final Project project) {
        if (project == null) throw new EmptyProjectException();
        projectRepository.delete(project);
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final Long userId,
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @Nullable Project project = projectRepository.removeOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final Long userId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Project project = projectRepository.removeOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    @Transactional
    public void updateProjectById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project); //merge
    }

}
