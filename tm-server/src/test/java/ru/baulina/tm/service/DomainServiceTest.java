package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import org.mockito.Mockito;
import ru.baulina.tm.UnitServiceTest;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.api.service.IDomainService;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.dto.DomainDTO;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import java.util.ArrayList;

import static org.mockito.Mockito.*;

@Category({UnitTest.class, UnitServiceTest.class})
public class DomainServiceTest {

//    @NotNull
//    private static final Domain domain = new Domain();
//    @NotNull private static Task task;
//    @NotNull private static Project project;
//    @NotNull private static User userTest;
//
//    @NotNull private static ArrayList<User> users = new ArrayList<>();
//    @NotNull private static ArrayList<Task> tasks = new ArrayList<>();
//    @NotNull private static ArrayList<Project> projects = new ArrayList<>();
//
//    @Rule
//    @NotNull
//    public final TestName testName = new TestName();
//
//    IUserService mUserService = Mockito.mock(IUserService.class);
//    IProjectService mProjectService = Mockito.mock(IProjectService.class);
//    ITaskService mTaskService = Mockito.mock(ITaskService.class);
//
//    IDomainService domainService = new DomainService(mUserService, mProjectService, mTaskService);
//
//    @BeforeClass
//    public static void setUsers() throws Exception {
//        userTest = new User();
//        userTest.setId(12345L);
//        userTest.setLogin("test");
//        userTest.setPasswordHash("14jhd54");
//        userTest.setRole(Role.USER);
//
//        task = new Task();
//        task.setId(54782L);
//        task.setUserId(12345L);
//        task.setName("task");
//
//        project = new Project();
//        project.setId(74125L);
//        project.setUserId(12345L);
//        project.setName("project");
//
//        users = new ArrayList<>();
//        users.add(userTest);
//        tasks = new ArrayList<>();
//        tasks.add(task);
//        projects = new ArrayList<>();
//        projects.add(project);
//
//        domain.setUsers(users);
//        domain.setTasks(tasks);
//        domain.setProjects(projects);
//    }
//
//    @Before
//    public void setTestName() throws Exception {
//        System.out.println(testName.getMethodName());
//    }
//
//    @Test
//    public void testLoad() {
//        doNothing().when(mUserService).load(users);
//        doNothing().when(mProjectService).load(projects);
//        doNothing().when(mTaskService).load(tasks);
//        domainService.load(domain);
//        verify(mUserService).load(users);
//        verify(mProjectService).load(projects);
//        verify(mTaskService).load(tasks);
//    }
//
//    @Test
//    public void testLoadNullDomain() {
//        doNothing().when(mUserService).load(users);
//        doNothing().when(mProjectService).load(projects);
//        doNothing().when(mTaskService).load(tasks);
//        domainService.load(null);
//        verify(mUserService, never()).load(users);
//        verify(mProjectService, never()).load(projects);
//        verify(mTaskService, never()).load(tasks);
//    }
//
//    @Test
//    public void testExport() {
//        when(mUserService.getList()).thenReturn(users);
//        when(mProjectService.getList()).thenReturn(projects);
//        when(mTaskService.getList()).thenReturn(tasks);
//        domainService.export(domain);
//        verify(mUserService).getList();
//        verify(mProjectService).getList();
//        verify(mTaskService).getList();
//    }
//
//    @Test
//    public void testExportNullDomain() {
//        when(mUserService.getList()).thenReturn(users);
//        when(mProjectService.getList()).thenReturn(projects);
//        when(mTaskService.getList()).thenReturn(tasks);
//        domainService.export(null);
//        verify(mUserService, never()).getList();
//        verify(mProjectService, never()).getList();
//        verify(mTaskService, never()).getList();
//    }

}