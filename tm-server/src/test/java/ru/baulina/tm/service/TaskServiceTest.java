package ru.baulina.tm.service;

import org.junit.experimental.categories.Category;
import ru.baulina.tm.UnitServiceTest;
import ru.baulina.tm.UnitTest;

@Category({UnitTest.class, UnitServiceTest.class})
public class TaskServiceTest {

//    @NotNull private static Task task;
//    @NotNull private static User userTest;
//
//    @Rule
//    @NotNull
//    public final TestName testName = new TestName();
//
//    ITaskRepository mTaskRepository = Mockito.mock(ITaskRepository.class);
//
//    @InjectMocks
//    private final ITaskService taskService = new TaskService(mTaskRepository);
//
//    @Before
//    public void setTestName() throws Exception {
//        System.out.println(testName.getMethodName());
//    }
//
//    @BeforeClass
//    public static void setUsers() throws Exception {
//        userTest = new User();
//        userTest.setId(12345L);
//        userTest.setLogin("test");
//        userTest.setPasswordHash("14jhd54");
//        userTest.setRole(Role.USER);
//
//        task = new Task();
//        task.setId(22222L);
//        task.setUserId(12345L);
//        task.setName("task");
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testCreateWithNullUserId() {
//        taskService.create(null, "task");
//        taskService.create(null, "task", "description");
//    }
//
//    @Test (expected = EmptyNameException.class)
//    public void testCreateWithNullName() {
//        taskService.create(123L, null);
//        taskService.create(123L, null, "description");
//    }
//
//    @Test (expected = EmptyNameException.class)
//    public void testCreateWithEmptyName() {
//        taskService.create(123L, "");
//        taskService.create(123L, "", "description");
//    }
//
//    @Test
//    public void testCreate() {
//        doNothing().when(mTaskRepository).add(eq(123L), isA(Task.class));
//        taskService.create(123L, "newTask");
//        ArgumentCaptor<Task> taskCaptor = ArgumentCaptor.forClass(Task.class);
//        verify(mTaskRepository).add(eq(123L), taskCaptor.capture());
//        Task createdTask = taskCaptor.getValue();
//        Assert.assertEquals("newTask", createdTask.getName());
//    }
//
//    @Test
//    public void testCreateWithDescription() {
//        doNothing().when(mTaskRepository).add(eq(12345L), isA(Task.class));
//        taskService.create(12345L, "newTask","description");
//        ArgumentCaptor<Task> taskCaptor = ArgumentCaptor.forClass(Task.class);
//        verify(mTaskRepository).add(eq(12345L), taskCaptor.capture());
//        Task createdTask = taskCaptor.getValue();
//        Assert.assertEquals("newTask", createdTask.getName());
//        Assert.assertEquals("description", createdTask.getDescription());
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testAddWithNullUserId() {
//        taskService.add(null, task);
//    }
//
//    @Test
//    public void testAdd() {
//        doNothing().when(mTaskRepository).add(12345L, task);
//        taskService.add(12345L, task);
//        verify(mTaskRepository).add(12345L, task);
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testRemoveWithNullUserId() {
//        taskService.remove(null, task);
//    }
//
//    @Test
//    public void testRemove() {
//        doNothing().when(mTaskRepository).remove(12345L, task);
//        taskService.remove(12345L, task);
//        verify(mTaskRepository).remove(12345L, task);
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testFindAllWithNullUserId() {
//        taskService.findAll(null);
//    }
//
//    @Test
//    public void testFindAll() {
//        @NotNull ArrayList<Task> tasks = new ArrayList<>();
//        tasks.add(task);
//        Task task1 = new Task();
//        task1.setUserId(12345L);
//        task1.setName("task");
//        tasks.add(task1);
//        when(mTaskRepository.findAll(12345L)).thenReturn(tasks);
//        Assert.assertEquals(tasks, taskService.findAll(12345L));
//        verify(mTaskRepository).findAll(12345L);
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testClearWithNullUserId() {
//        taskService.clear(null);
//    }
//
//    @Test
//    public void testClear() {
//        doNothing().when(mTaskRepository).clear(12345L);
//        taskService.clear(12345L);
//        verify(mTaskRepository).clear(12345L);
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testFindOneWithNullUserIdById() {
//        taskService.findOneById(null, 123L);
//    }
//
//    @Test (expected = EmptyIdException.class)
//    public void testFindOneWithNullIdById() {
//        taskService.findOneById(123L, null);
//    }
//
//    @Test
//    public void testFindOneById() {
//        when(mTaskRepository.findOneById(12345L, 22222L)).thenReturn(task);
//        Assert.assertEquals(task, taskService.findOneById(12345L, 22222L));
//        verify(mTaskRepository).findOneById(12345L, 22222L);
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testFindOneWithNullUserIdByIndex() {
//        taskService.findOneByIndex(null, 0);
//    }
//
//    @Test (expected = IncorrectIndexException.class)
//    public void testFindOneWithNullIndexByIndex() {
//        taskService.findOneByIndex(123L, null);
//    }
//
//    @Test
//    public void testFindOneByIndex() {
//        when(mTaskRepository.findOneByIndex(12345L, 0)).thenReturn(task);
//        Assert.assertEquals(task, taskService.findOneByIndex(12345L, 0));
//        verify(mTaskRepository).findOneByIndex(12345L, 0);
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testFindOneWithNullUserIdByName() {
//        taskService.findOneByName(null, "task");
//    }
//
//    @Test (expected = EmptyNameException.class)
//    public void testFindOneWithNullNameByName() {
//        taskService.findOneByName(123L, null);
//    }
//
//    @Test
//    public void testFindOneByName() {
//        when(mTaskRepository.findOneByName(12345L, "task")).thenReturn(task);
//        Assert.assertEquals(task, taskService.findOneByName(12345L, "task"));
//        verify(mTaskRepository).findOneByName(12345L, "task");
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testRemoveOneWithNullUserIdById() {
//        taskService.removeOneById(null, 123L);
//    }
//
//    @Test (expected = EmptyIdException.class)
//    public void testRemoveOneWithNullIdById() {
//        taskService.removeOneById(123L, null);
//    }
//
//    @Test
//    public void testRemoveOneById() {
//        when(mTaskRepository.removeOneById(12345L, 22222L)).thenReturn(task);
//        Assert.assertEquals(task, taskService.removeOneById(12345L, 22222L));
//        verify(mTaskRepository).removeOneById(12345L, 22222L);
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testRemoveOneWithNullUserIdByIndex() {
//        taskService.removeOneByIndex(null, 0);
//    }
//
//    @Test (expected = IncorrectIndexException.class)
//    public void testRemoveOneWithNullIndexByIndex() {
//        taskService.removeOneByIndex(123L, null);
//    }
//
//    @Test
//    public void testRemoveOneByIndex() {
//        when(mTaskRepository.removeOneByIndex(12345L, 0)).thenReturn(task);
//        Assert.assertEquals(task, taskService.removeOneByIndex(12345L, 0));
//        verify(mTaskRepository).removeOneByIndex(12345L, 0);
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testRemoveOneWithNullUserIdByName() {
//        taskService.removeOneByName(null, "task");
//    }
//
//    @Test (expected = EmptyNameException.class)
//    public void testRemoveOneWithNullNameByName() {
//        taskService.removeOneByName(123L, null);
//    }
//
//    @Test
//    public void testRemoveOneByName() {
//        when(mTaskRepository.removeOneByName(12345L, "task")).thenReturn(task);
//        Assert.assertEquals(task, taskService.removeOneByName(12345L, "task"));
//        verify(mTaskRepository).removeOneByName(12345L, "task");
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testUpdateTaskWithNullUserIdById() {
//        taskService.updateTaskById(null, 22222L, "taskNew", "description");
//    }
//
//    @Test (expected = EmptyIdException.class)
//    public void testUpdateTaskWithNullIdById() {
//        taskService.updateTaskById(12345L, null, "taskNew", "description");
//    }
//
//    @Test (expected = EmptyNameException.class)
//    public void testUpdateTaskWithNullNameById() {
//        taskService.updateTaskById(12345L, 22222L, null, "description");
//        taskService.updateTaskById(12345L, 22222L, "", "description");
//    }
//
//    @Test
//    public void updateTaskById() {
//        when(mTaskRepository.findOneById(12345L, 22222L)).thenReturn(task);
//        doNothing().when(mTaskRepository).merge(task);
//        Assert.assertEquals(
//                task, taskService.updateTaskById(12345L, 22222L, "taskNew", "description")
//        );
//        verify(mTaskRepository).merge(task);
//        verify(mTaskRepository).findOneById(12345L, 22222L);
//        Assert.assertEquals(22222L, task.getId());
//        Assert.assertEquals("taskNew", task.getName());
//        Assert.assertEquals("description", task.getDescription());
//    }
//
//    @Test (expected = EmptyUserIdException.class)
//    public void testUpdateTaskWithNullUserIdByIndex() {
//        taskService.updateTaskByIndex(null, 0, "taskNew", "description");
//    }
//
//    @Test (expected = IncorrectIndexException.class)
//    public void testUpdateTaskWithNullIdByIndex() {
//        taskService.updateTaskByIndex(12345L, null, "taskNew", "description");
//    }
//
//    @Test (expected = EmptyNameException.class)
//    public void testUpdateTaskWithNullNameByIndex() {
//        taskService.updateTaskByIndex(12345L, 0, null, "description");
//        taskService.updateTaskByIndex(12345L, 0, "", "description");
//    }
//
//    @Test
//    public void testUpdateTaskByIndex() {
//        when(mTaskRepository.findOneByIndex(12345L, 0)).thenReturn(task);
//        doNothing().when(mTaskRepository).merge(task);
//        @Nullable final Task expectedTask = taskService.updateTaskByIndex(
//                12345L, 0, "taskNew", "description"
//        );
//        Assert.assertEquals(task, expectedTask);
//        verify(mTaskRepository).merge(task);
//        verify(mTaskRepository).findOneByIndex(12345L, 0);
//        Assert.assertEquals("taskNew", task.getName());
//        Assert.assertEquals("description", task.getDescription());
//    }

}