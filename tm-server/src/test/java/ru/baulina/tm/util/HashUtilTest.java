package ru.baulina.tm.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.UnitServiceTest;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.UnitUtilTest;

@Category({UnitTest.class, UnitUtilTest.class})
public class HashUtilTest {

    @Rule
    public final TestName testName = new TestName();

    @Before
    public void setTestName() throws Exception {
        System.out.println(testName.getMethodName());
    }

    @Test
    public void testSalt() {
        final String value = "password";
        final String expected = "9cec6ef5bb9f11cd2ae14722d20925b3";
        Assert.assertNotNull(expected, HashUtil.salt(value));
    }

    @Test
    public void testMd5hashing() {
        final String value = "password";
        final String expected = "23fd42ae8029968e3f594377b71c4c19";
        Assert.assertEquals(expected, HashUtil.salt(value));
    }
}