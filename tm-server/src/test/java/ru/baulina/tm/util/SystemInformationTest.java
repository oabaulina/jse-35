package ru.baulina.tm.util;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import ru.baulina.tm.UnitTest;
import ru.baulina.tm.UnitUtilTest;

@Category({UnitTest.class, UnitUtilTest.class})
public class SystemInformationTest {

    @Rule
    public final TestName testName = new TestName();

    @Before
    public void setTestName() throws Exception {
        System.out.println(testName.getMethodName());
    }

    @Test
    public void testGetAvailableProcessors() {
        Assert.assertTrue(SystemInformation.getAvailableProcessors() > 0);
    }

    @Test
    public void testGetFreeMemory() {
        Assert.assertTrue(SystemInformation.getFreeMemory() > 0);
    }

    @Test
    public void testGetMaxMemory() {
        Assert.assertTrue(SystemInformation.getMaxMemory() > 0);
    }

    @Test
    public void testGetTotalMemory() {
        Assert.assertTrue(SystemInformation.getTotalMemory() > 0);
    }

    @Test
    public void testGetUsedMemoryFormat() {
        Assert.assertTrue(SystemInformation.getUsedMemoryFormat() >= 0);
        Assert.assertTrue(SystemInformation.getUsedMemoryFormat() <= SystemInformation.getTotalMemory());
    }

}