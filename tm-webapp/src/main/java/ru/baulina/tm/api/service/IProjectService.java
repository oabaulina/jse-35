package ru.baulina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.model.Project;
import ru.baulina.tm.model.User;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    Project create(@Nullable final User user, @Nullable final String name);

    @Nullable
    Project create(
            @Nullable final User user, @Nullable final String name,
            @Nullable final String description
    );

    void remove(@Nullable final Project project);

    @Nullable
    List<Project> findAll(@Nullable final Long userId);

    void clear(@Nullable final Long userId);

    @Nullable List<Project> findListProjects();

    @Nullable
    Project findOneById(@Nullable final Long userId, @Nullable final Long id);

    @Nullable
    Project findOneByName(@Nullable final Long userId, @Nullable final String name);

    void removeOneById(@Nullable final Long userId, @Nullable final Long id);

    void removeOneByName(@Nullable final Long userId, @Nullable final String name);

    void updateProjectById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String  description
    );

}
