package ru.baulina.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    @Nullable String getServiceHost();

    @Nullable Integer getServicePort();

   @Nullable String getSessionSalt();

    @Nullable Integer getSessionCycle();

}
