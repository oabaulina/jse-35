package ru.baulina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    @Nullable
    User create(@Nullable final String login, @Nullable final String password);

    @Nullable
    User create(@Nullable final String login, @Nullable final String password, @Nullable final String email);

    @Nullable
    User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role);

    @Nullable List<User> findListUsers();

    @Nullable
    User findUser(
            @Nullable final String login,
            @Nullable final String password
    );

    @Nullable
    User findById(@Nullable final Long id);

    @Nullable
    User findByLogin(@Nullable final String login);

    void removeUser(@Nullable final User user);

    void removeById(@Nullable final Long id);

    void removeByLogin(@Nullable final String login);

    void lockUserLogin(@Nullable final String login);

    void unlockUserLogin(@Nullable final String login);

    void changePassword(
            @Nullable final String passwordOld,
            @Nullable final String passwordNew,
            @Nullable final Long userId
    );

    void changeUser(
            @Nullable final String email,
            @Nullable final String festName,
            @Nullable final String LastName,
            @Nullable final Long userId
    );

}
