package ru.baulina.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.model.User;
import ru.baulina.tm.util.HashUtil;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    private final IUserService userService;

    @Autowired
    public LoginController(
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String login() {
        return "/login";
    }

    @PostMapping("/login")
    public ModelAndView postLogin(HttpServletRequest req, HttpServletResponse resp) {
        @NotNull final String login = req.getParameter("login");
        @NotNull final String psw = req.getParameter("psw");

        System.out.println("login controller: ");
        System.out.println("username: " + login);
        System.out.println("password: " + psw);

        @Nullable final User currentUser = userService.findByLogin(login);
        System.out.println("user: " + currentUser);

        if (currentUser == null) {
            System.out.println("user not found, redirect to /login");
            return new ModelAndView("/login","errorMessage","Wrong LOGIN!");
        }

        @NotNull final String passwordHash = currentUser.getPasswordHash();
        @NotNull final String pswdHash = HashUtil.salt(psw);
        System.out.println("passwordHash: " + passwordHash);
        System.out.println("pswdHash: " + pswdHash);

        if (!pswdHash.equals(passwordHash)) {
            return new ModelAndView("/login","errorMessage","Wrong PASSWORD!");
        }

        @NotNull HttpSession session = req.getSession();
        session.setAttribute("login", login);
        session.setAttribute("user", currentUser);
        session.setAttribute("userId", currentUser.getId());
        session.setMaxInactiveInterval(30*60);
        return new ModelAndView("redirect:/users");
    }

}
