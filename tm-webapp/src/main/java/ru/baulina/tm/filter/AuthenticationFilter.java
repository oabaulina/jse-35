package ru.baulina.tm.filter;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {}

    @Override
    public void destroy() {}

    @Override
    public void doFilter(
            @NotNull final ServletRequest request,
            @NotNull final ServletResponse response,
            @NotNull final FilterChain chain
    ) throws IOException, ServletException {
        @NotNull final HttpServletRequest httpRequest = (HttpServletRequest) request;
        @NotNull final HttpServletResponse httpResponse = (HttpServletResponse) response;

        @Nullable final HttpSession session = httpRequest.getSession(false);

        @NotNull final String requestURI = httpRequest.getRequestURI();
        @NotNull final String loginURI = httpRequest.getContextPath() + "/login";
        @NotNull final String registerURI = httpRequest.getContextPath() + "/register";

        @NotNull final Boolean isLoggedIn = (session != null && session.getAttribute("userId") != null);
        @NotNull final Boolean isLoginPageRequest = loginURI.equals(requestURI);
        @NotNull final Boolean isRegisterPageRequest = registerURI.equals(requestURI);

        @NotNull final Boolean isResource = requestURI.startsWith("/css") || requestURI.startsWith("/images");

        if (requestURI.equals("/") || isResource || isLoggedIn || isLoginPageRequest || isRegisterPageRequest) {
            chain.doFilter(request, response);
        } else {
            httpResponse.sendRedirect(loginURI);
        }
    }

}
