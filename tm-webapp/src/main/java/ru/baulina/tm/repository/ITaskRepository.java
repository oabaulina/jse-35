package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.baulina.tm.model.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends IRepository<Task> {

    void deleteByUserId(@NotNull final Long userId);

    default
    void clear(@NotNull final Long userId) {
        deleteByUserId(userId);
    };

    @NotNull
    List<Task> findByUserId(@NotNull final Long userId);

    default
    @NotNull
    List<Task> findAllByUserId(@NotNull final Long userId) {
        return findByUserId(userId);
    };

    default
    @NotNull
    List<Task> findListTasks() {
        return findAll();
    };

    @Nullable
    Task findByUserIdAndId(
            @NotNull final Long userId,
            @NotNull final Long id
    );

    default
    @Nullable
    Task findOneById(
            @NotNull final Long userId,
            @NotNull final Long id
    ) {
        return findByUserIdAndId(userId, id);
    };

    @Nullable
    Task findByUserIdAndName(
            @NotNull final Long userId,
            @NotNull final String name
    );

    default
    @Nullable
    Task findOneByName(
            @NotNull final Long userId,
            @NotNull final String name
    ) {
        return findByUserIdAndName(userId, name);
    };

    @Nullable
    Task deleteByUserIdAndId(
            @NotNull final Long userId,
            @NotNull final Long id
    );

    default
    @Nullable
    Task removeOneById(
            @NotNull final Long userId,
            @NotNull final Long id
    ) {
        return deleteByUserIdAndId(userId, id);
    };

    @Nullable
    Task deleteByUserIdAndName(
            @NotNull final Long userId,
            @NotNull final String name
    );

    default
    @Nullable
    Task removeOneByName(
            @NotNull final Long userId,
            @NotNull final String name
    ) {
        return deleteByUserIdAndName(userId, name);
    }

}
