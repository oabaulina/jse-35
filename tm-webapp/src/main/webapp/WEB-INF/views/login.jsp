<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="${pageContext.request.contextPath}/css/style-login.css" rel="stylesheet">
    </head>
    <body>

        <c:if test="${errorMessage}">
            <div class="container">${errorMessage}</div>
        </c:if>


        <form class="modal-content" action="/login" method="post">

            <div class="imgcontainer">
                  <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">×</span>
                  <img src="${pageContext.request.contextPath}/images/img_avatar.png" alt="Avatar" class="avatar">
            </div>

            <div class="container">
                  <label for="uname"><b>Username</b></label>
                  <input type="text" placeholder="Enter Username" name="login" required>

                  <label for="psw"><b>Password</b></label>
                  <input type="password" placeholder="Enter Password" name="psw" required>

                  <button type="submit">Login</button>
            </div>

            <div class="container" style="background-color:#f1f1f1">
                <a href="/" class="cancelbtn">Cancel</a>
            </div>
        </form>

    </body>
</html>