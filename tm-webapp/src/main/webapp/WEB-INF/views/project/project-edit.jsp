<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>
<jsp:include page="../../include/_header.jsp"/>
<div style="padding-left:16px">
  <h2>PROJECT EDIT</h2>
</div>

<form action="/project/edit" modelAttribute="project" method="POST">
    <div class="container-edit">
        <hr>
            <input type="hidden" name="id" value= "${project.id}" />

            <label for="email"><b>Name</b></label>
            <input type="text" name="name" value="${project.name}" required>

            <label for="psw"><b>Description</b></label>
            <input type="text" name="description" value="${project.description}" required>

            <button type="submit" class="savebtn">SAVE PROJECT</button>
        </hr>
    </div>
</form>

<jsp:include page="../../include/_footer.jsp"/>

